{# This is the template file to edit, ignore the warning below #}
#
# THIS FILE IS GENERATED, DO NOT EDIT
#

################################################################################
#
# {{distribution.title()}} checks
#
################################################################################

include:
  - local: '/templates/{{distribution}}.yml'


stages:
  - {{distribution}}_container_build
  - {{distribution}}_check


#
# Common variable definitions
#
.ci-image-{{distribution}}:
  image: $CI_REGISTRY_IMAGE/container-build-base:{{container_build_base_tag}}

.ci-variables-{{distribution}}:
  variables:
    FDO_DISTRIBUTION_PACKAGES: 'wget curl'
    FDO_DISTRIBUTION_EXEC: 'sh test/script.sh'
  {% if not fixed_version %}
    FDO_DISTRIBUTION_VERSION: '{{versions[0]}}'
  {% endif %}
    FDO_EXPIRES_AFTER: '1h'
    FDO_CBUILD: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/cbuild/{{ci_fairy.sha256sum('./bootstrap/cbuild')}}/cbuild

.ci-variables-{{distribution}}@x86_64:
  extends: .ci-variables-{{distribution}}
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-x86_64-$CI_PIPELINE_ID

.ci-variables-{{distribution}}@aarch64:
  extends: .ci-variables-{{distribution}}
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-aarch64-$CI_PIPELINE_ID

{% if templates.container %}
#
# A few templates to avoid writing the image and stage in each job
#
.{{distribution}}:ci@container-build@x86_64:
  extends:
    - .fdo.container-build@{{distribution}}
    - .ci-variables-{{distribution}}@x86_64
    - .ci-image-{{distribution}}
  stage: {{distribution}}_container_build

{% if aarch64 %}

.{{distribution}}:ci@container-build@aarch64:
  extends:
    - .fdo.container-build@{{distribution}}
    - .ci-variables-{{distribution}}@aarch64
    - .ci-image-{{distribution}}
  tags:
    - aarch64
  stage: {{distribution}}_container_build

{% endif %}
{% endif %}
{% if templates.qemu %}

#
# Qemu build
#
.{{distribution}}:ci@qemu-build@x86_64:
  extends:
    - .fdo.qemu-build@{{distribution}}@x86_64
    - .ci-variables-{{distribution}}@x86_64
    - .ci-image-{{distribution}}
  image: $CI_REGISTRY_IMAGE/qemu-build-base:{{qemu_tag}}
  stage: {{distribution}}_container_build
  artifacts:
    name: logs-$CI_PIPELINE_ID
    when: always
    expire_in: 1 week
    paths:
      - console.out
{% endif %}

{% if templates.container %}
#
# generic {{distribution}} checks
#
.{{distribution}}@check@x86_64:
  extends:
    - .ci-variables-{{distribution}}@x86_64
    - .fdo.distribution-image@{{distribution}}
  stage: {{distribution}}_check
  script:
      # run both curl and wget because one of those two is installed and one is
      # in the base image, but it depends on the distro which one
    - curl --insecure https://gitlab.freedesktop.org
    - wget --no-check-certificate https://gitlab.freedesktop.org
      # make sure our test script has been run
    - if [[ -e /test_file ]] ;
      then
        echo $FDO_DISTRIBUTION_EXEC properly run ;
      else
        exit 1 ;
      fi
{% endif %}
{% if templates.qemu %}


.{{distribution}}@qemu-check@x86_64:
  stage: {{distribution}}_check
  extends:
    - .ci-variables-{{distribution}}@x86_64
    - .fdo.distribution-image@{{distribution}}
  tags:
    - kvm
  script:
    - pushd /app
      # start the VM
    - /app/vmctl start
      # make sure our FDO_DISTRIBUTION_EXEC script has been run
    - if /app/vmctl exec test -e /test_file ;
      then
        echo $FDO_DISTRIBUTION_EXEC properly run ;
      else
        exit 1 ;
      fi
      # run both curl and wget because one of those two is installed and one is
      # in the base image, but it depends on the distro which one
    - /app/vmctl exec curl --insecure https://gitlab.freedesktop.org
    - /app/vmctl exec wget --no-check-certificate https://gitlab.freedesktop.org
      # terminate the VM
    - /app/vmctl stop

      # start the VM, with the kernel parameters (if we have a kernel)
    - |
      [[ $(ls /app/vmlinuz*) ]] || exit 0
    - /app/vmctl start-kernel
      # make sure we can still use curl/wget
    - /app/vmctl exec curl --insecure https://gitlab.freedesktop.org
    - /app/vmctl exec wget --no-check-certificate https://gitlab.freedesktop.org
      # terminate the VM
    - /app/vmctl stop
  artifacts:
    name: logs-$CI_PIPELINE_ID
    when: always
    expire_in: 1 week
    paths:
      - console.out
{% endif %}


{% if templates.container %}
{# We only do the full test on the first version of the distribution #}
{# other versions have a slighter check at the end #}
#
# straight {{distribution}} build and test
#
{{distribution}}:{{versions[0]}}@container-build@x86_64:
  extends: .{{distribution}}:ci@container-build@x86_64


{{distribution}}:{{versions[0]}}@check@x86_64:
  extends: .{{distribution}}@check@x86_64
  needs:
    - {{distribution}}:{{versions[0]}}@container-build@x86_64

# Test FDO_BASE_IMAGE. We don't need to do much here, if our
# FDO_DISTRIBUTION_EXEC script can run curl+wget this means we're running on
# the desired base image. That's good enough.
{{distribution}}:{{versions[0]}}@base-image@x86_64:
  extends: {{distribution}}:{{versions[0]}}@container-build@x86_64
  stage: {{distribution}}_check
  variables:
    # We need to duplicate FDO_DISTRIBUTION_TAG here, gitlab doesn't allow nested expansion
    FDO_BASE_IMAGE: registry.freedesktop.org/$CI_PROJECT_PATH/{{distribution}}/{{versions[0]}}:fdo-ci-x86_64-$CI_PIPELINE_ID
    FDO_DISTRIBUTION_PACKAGES: ''
    FDO_DISTRIBUTION_EXEC: 'test/test-wget-curl.sh'
    FDO_FORCE_REBUILD: 1
    FDO_DISTRIBUTION_TAG: fdo-ci-baseimage-x86_64-$CI_PIPELINE_ID
  needs:
    - {{distribution}}:{{versions[0]}}@container-build@x86_64

#
# /cache {{distribution}} check (in build stage)
#
# Also ensures setting FDO_FORCE_REBUILD will do the correct job
#
{{distribution}}@cache-container-build@x86_64:
  extends: .{{distribution}}:ci@container-build@x86_64
  before_script:
      # The template normally symlinks the /cache
      # folder, but we want a fresh new one for the
      # tests.
    - mkdir runner_cache_$CI_PIPELINE_ID
    - uname -a | tee runner_cache_$CI_PIPELINE_ID/foo-$CI_PIPELINE_ID

  artifacts:
    paths:
      - runner_cache_$CI_PIPELINE_ID/*
    expire_in: 1 week

  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-cache-x86_64-$CI_PIPELINE_ID
    FDO_DISTRIBUTION_EXEC: 'bash test/test_cache.sh $CI_PIPELINE_ID'
    FDO_CACHE_DIR: $CI_PROJECT_DIR/runner_cache_$CI_PIPELINE_ID
    FDO_FORCE_REBUILD: 1

#
# /cache {{distribution}} check (in check stage)
#
{{distribution}}@cache-check@x86_64:
  stage: {{distribution}}_check
  image: alpine:latest
  script:
    # in the previous stage ({{distribution}}@cache-container-build@x86_64),
    # test/test_cache.sh checked for the existance of `/cache/foo-$CI_PIPELINE_ID`
    # and if it found it, it wrote `/cache/bar-$CI_PIPELINE_ID`.
    #
    # So if we have in the artifacts `bar-$CI_PIPELINE_ID`, that means
    # 2 things:
    # - /cache was properly mounted while building the container
    # - the $FDO_CACHE_DIR has been properly written from within the
    #   building container, meaning the /cache folder has been successfully
    #   updated.
    - if [ -e $CI_PROJECT_DIR/runner_cache_$CI_PIPELINE_ID/bar-$CI_PIPELINE_ID ] ;
      then
        echo Successfully read/wrote the cache folder, all good ;
      else
        echo FAILURE while retrieving the previous artifacts ;
        exit 1 ;
      fi
  needs:
    - job: {{distribution}}@cache-container-build@x86_64
      artifacts: true

#
# FDO_USER {{distribution}} check (in build stage)
#
# Also ensures setting FDO_FORCE_REBUILD will do the correct job
#
{{distribution}}@fdo-user-container-build@x86_64:
  extends: .{{distribution}}:ci@container-build@x86_64
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-fdo-user-x86_64-$CI_PIPELINE_ID
    FDO_FORCE_REBUILD: 1
    FDO_USER: testuser

#
# FDO_USER {{distribution}} check (in check stage)
#
{{distribution}}@fdo-user-check@x86_64:
  extends:
    - .ci-variables-{{distribution}}@x86_64
    - .fdo.distribution-image@{{distribution}}
  stage: {{distribution}}_check
  script:
    - if [ $(whoami) != "testuser" ]; then
        echo "Wrong username $(whoami)" ;
        exit 1 ;
      fi
    - if [ $HOME != "/home/testuser" ]; then
        echo "Wrong HOME env $HOME" ;
        exit 1 ;
      fi
    # Make sure workdir and $HOME are writable by testuser
    - touch testfile
    - touch $HOME/testfile
  needs:
    - job: {{distribution}}@fdo-user-container-build@x86_64
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-fdo-user-x86_64-$CI_PIPELINE_ID

{% if aarch64 %}


{{distribution}}:{{versions[0]}}@container-build@aarch64:
  extends: .{{distribution}}:ci@container-build@aarch64
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-aarch64-$CI_PIPELINE_ID


{{distribution}}:{{versions[0]}}@check@aarch64:
  extends: .{{distribution}}@check@x86_64
  tags:
    - aarch64
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-aarch64-$CI_PIPELINE_ID
  needs:
    - {{distribution}}:{{versions[0]}}@container-build@aarch64
{% endif %}


#
# make sure we do not rebuild the image if the tag exists (during the check)
#
do not rebuild {{distribution}}:{{versions[0]}}@container-build@x86_64:
  extends: .{{distribution}}:ci@container-build@x86_64
  stage: {{distribution}}_check
  variables:
    FDO_UPSTREAM_REPO: $CI_PROJECT_PATH
    FDO_DISTRIBUTION_PACKAGES: 'this-package-should-not-exist'
  needs:
    - {{distribution}}:{{versions[0]}}@container-build@x86_64


#
# check if the labels were correctly applied
#
check labels {{distribution}}@x86_64:{{versions[0]}}:
  extends:
    - {{distribution}}:{{versions[0]}}@check@x86_64
  image: $CI_REGISTRY_IMAGE/container-build-base:{{container_build_base_tag}}
  script:
    # FDO_DISTRIBUTION_IMAGE still has indirections
    - DISTRO_IMAGE=$(eval echo ${FDO_DISTRIBUTION_IMAGE})

    # retrieve the infos from the registry (once)
    - JSON_IMAGE=$(skopeo inspect docker://$DISTRO_IMAGE)

    # parse all the labels we care about
    - IMAGE_PIPELINE_ID=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.pipeline_id"]')
    - IMAGE_JOB_ID=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.job_id"]')
    - IMAGE_PROJECT=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.project"]')
    - IMAGE_COMMIT=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.commit"]')

    # some debug information
    - echo $JSON_IMAGE
    - echo $IMAGE_PIPELINE_ID $CI_PIPELINE_ID
    - echo $IMAGE_JOB_ID
    - echo $IMAGE_PROJECT $CI_PROJECT_PATH
    - echo $IMAGE_COMMIT $CI_COMMIT_SHA

    # ensure the labels are correct (we are on the same pipeline)
    - '[[ x"$IMAGE_PIPELINE_ID" == x"$CI_PIPELINE_ID" ]]'
    - '[[ x"$IMAGE_JOB_ID" != x"" ]]' # we don't know the job ID, but it must be set
    - '[[ x"$IMAGE_PROJECT" == x"$CI_PROJECT_PATH" ]]'
    - '[[ x"$IMAGE_COMMIT" == x"$CI_COMMIT_SHA" ]]'
  needs:
    - {{distribution}}:{{versions[0]}}@container-build@x86_64


#
# make sure we do not rebuild the image if the tag exists in the upstream
# repository (during the check)
# special case where FDO_REPO_SUFFIX == ci_templates_test_upstream
#
pull upstream {{distribution}}:{{versions[0]}}@container-build@x86_64:
  extends: .{{distribution}}:ci@container-build@x86_64
  stage: {{distribution}}_check
  variables:
    FDO_UPSTREAM_REPO: $CI_PROJECT_PATH
    FDO_REPO_SUFFIX: {{distribution}}/ci_templates_test_upstream
    FDO_DISTRIBUTION_PACKAGES: 'this-package-should-not-exist'
  needs:
    - {{distribution}}:{{versions[0]}}@container-build@x86_64
{% if versions[1:] %}

#
# Try our {{distribution}} scripts with other versions and check
#
{% for v in versions[1:] %}

{{distribution}}:{{v}}@container-build@x86_64:
  extends: .{{distribution}}:ci@container-build@x86_64
  variables:
    FDO_DISTRIBUTION_VERSION: '{{v}}'

{{distribution}}:{{v}}@check@x86_64:
  extends: .{{distribution}}@check@x86_64
  variables:
    FDO_DISTRIBUTION_VERSION: '{{v}}'
  needs:
    - {{distribution}}:{{v}}@container-build@x86_64
{% endfor %}
{% endif %}
{% endif %}
{% if templates.qemu %}

{{distribution}}:{{versions[0]}}@qemu-build@x86_64:
  extends: .{{distribution}}:ci@qemu-build@x86_64
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-qemu-x86_64-$CI_PIPELINE_ID
    FDO_DISTRIBUTION_PACKAGES: 'wget curl'
    FDO_CI_TEMPLATES_QEMU_BASE_IMAGE: $CI_REGISTRY_IMAGE/qemu-base:{{qemu_tag}}


{{distribution}}:{{versions[0]}}@qemu-check@x86_64:
  extends: .{{distribution}}@qemu-check@x86_64
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-qemu-x86_64-$CI_PIPELINE_ID
  needs:
    - {{distribution}}:{{versions[0]}}@qemu-build@x86_64
{% endif %}
