#!/bin/bash

set -e
set -x

HOME=${HOME:=/root}
WORKDIR=${WORKDIR:=/app}
CLOUD_IMAGE_URL="https://download.fedoraproject.org/pub/fedora/linux/releases/35/Cloud/x86_64/images/Fedora-Cloud-Base-35-1.2.x86_64.raw.xz"

if ! [[ -f $WORKDIR/vmctl ]]; then
    echo "$WORKDIR/vmctl script not found, cannot continue"
    exit 1
fi

pushd $WORKDIR

if ! [[ -e $WORKDIR/image.raw.xz ]]; then
    curl -L $CLOUD_IMAGE_URL -o $WORKDIR/image.raw.xz
fi

# create a common ssh key that will be used to generate the final VM images
ssh-keygen -t ed25519 -f $HOME/.ssh/ci-templates-vm -N ''

# to start the cloud-init ready image we need to provide it some input:
# https://blog.christophersmart.com/2016/06/17/booting-fedora-24-cloud-image-with-kvm/

cat > $WORKDIR/meta-data << EOF
instance-id: Cloud00
local-hostname: cloud-00
EOF


cat > $WORKDIR/user-data << EOF
#cloud-config
# Set the default user
system_info:
  default_user:
    name: cloud

# Unlock the default and root users
chpasswd:
  list: |
     cloud:password
     root:root
  expire: False

# Other settings
resize_rootfs: True
ssh_pwauth: True
disable_root: false
timezone: UTC

# Add any ssh public keys
ssh_authorized_keys:
 - $(cat $HOME/.ssh/ci-templates-vm.pub)

bootcmd:
 - [ sh, -c, echo "=========bootcmd=========" ]

runcmd:
 - [ sh, -c, echo "=========runcmd=========" ]

final_message: "SYSTEM READY TO LOG IN"
EOF

genisoimage -output $WORKDIR/my-seed.iso \
            -volid cidata \
            -joliet \
            -rock $WORKDIR/user-data $WORKDIR/meta-data

# do some initial preparation in the target VM so it is mkosi capable
$WORKDIR/vmctl start -cdrom $WORKDIR/my-seed.iso

# install mkosi dependencies to build up our final VM image
$WORKDIR/vmctl exec dnf install -y mkosi systemd-container git-core

# stop the vm and compress the image file
$WORKDIR/vmctl stop

# manually compress the image with `-T0` to use multithreading
xz -T0 $WORKDIR/image.raw

popd
